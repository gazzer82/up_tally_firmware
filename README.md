# UpTallyFirmware

**Description**

Firmware for running the UpTally phoenix application on a RPI4 or Grisp2 board https://www.grisp.org

## Elixir and Nerves

This software is written in [Elixir](https://elixir-lang.org), so you will need it installed on your system.

- Install 7Z as per the Burrito requirements [Burrito](https://github.com/burrito-elixir/burrito?tab=readme-ov-file#mix-release-config-options)
- Install [ASDF](https://asdf-vm.com/guide/getting-started.html)
- Install [Erlang ASDF Plugin](https://github.com/asdf-vm/asdf-erlang)
- Install [Elixir ASDF Plugin](https://github.com/asdf-vm/asdf-elixir)
- Install [ZIG ASDF Plugin](https://github.com/asdf-community/asdf-zig)

You will also need [Nerves](https://nerves-project.org) installed, follow the steps [here](https://hexdocs.pm/nerves/installation.html), pleaae make sure you complete all the steps on the page, even the ones right at the bottom!

## Firmware and code

To sucessfully build the firmware for installation you will need the two following git repositories cloned within the same main folder.

[up_tally_firmware](https://gitlab.com/gazzer82/up_tally_firmware) (this one) which provides the firmware to allow the application to be installed and run on the hardware.

[up_tally](https://gitlab.com/gazzer82/up-tally) which is the actual application itself.

### Setup up_tally

- Open up-tally folder
- Run `mix setup`

## Targets

Nerves applications produce images for hardware targets based on the `MIX_TARGET` environment variable. If `MIX_TARGET` is unset, `mix` builds an image that runs on the host (e.g., your laptop). This is useful for executing logic tests, running utilities, and debugging. Other targets are represented by a short name like `rpi3` that maps to a Nerves system image for that platform.

All of this logic is in the generated `mix.exs` and may be customized. For more information about targets see:

https://hexdocs.pm/nerves/targets.html#content

## To install the UpTallyFirmware app on a RPI4 CM board:

### Tools you will need:

- a CM4 device with eMMC

- a CM4 IO Board [raspberrypi.com/products/compute-module-4-i..](https://www.raspberrypi.com/products/compute-module-4-io-board/) or Tofu Board [https://store.oratek.com/products/tofu](https://store.oratek.com/products/tofu)

- For the CM4 IO Board, a Jumper 2-PIN like here: [sparkfun.com/products/9044](https://www.sparkfun.com/products/9044)

- For a CM4 IO Board, a 12V - 2.5A Power with 2.1mm Jack Barrel

- a Micro-USB to USB cable

### Software to Install

- libusb `brew install pkgconfig libusb`
- rpiboot `git clone https://github.com/raspberrypi/usbboot`

  - `cd usbboot`

  - `make`

  - This will compile `rpiboot` that we'll need later

Plug the CM4 to the IO Board as follow:

![img.jpg](https://cdn.hashnode.com/res/hashnode/image/upload/v1662739052705/7_JM9yWI8.jpg?auto=compress,format&format=webp)

For a CM4 IO Board, Apply the jumper on the J2 position as follow:

![img.jpg](https://cdn.hashnode.com/res/hashnode/image/upload/v1662739286603/GK0rxuk3J.jpg?auto=compress,format&format=webp)

This will prevent the CM4 booting from the eMMC.

Connect the USB-C port to your computer.

For the Tofu board to boot on eMMC, hold down the nRPIBOOT button while plugging in USB-C cable for flashing (no other power input). The USB cable will power the CM4 while it boots (more info on Tofu board instruction [here](https://tofu.oratek.com/#/?id=emmc-boot)), nPRIBOOT button located [here](https://tofu.oratek.com/#/?id=layout)

Now power on the CM4 IO Board, the D1 LED should turn on.

Launch rpiboot that you previously compiled `sudo ./rpiboot`, this will mount the eMMC volume on your computer as an external drive.

The D2 LED should turn on as well to indicate activity, and you should see the rpi4 internal storage mounted on your computer

Now we're ready to install our firmware to the eMMC.

1.  Open a terminal window and cd into the `up_tally_firmware` repository folder, and issue the following commands

2.  `export MIX_TARGET=rpi4`

3.  `mix deps.get`

4.  `mix firmware`

5.  `mix fimrware.burn` and press enter to select the storage and begin burning, watch out for a permission popup, if you do not see this, or get an error about unmounting the storage the burn has not completed so try again.

If you are unable to get the burn to work, you can create a disk image using the `mix firmware.image` command, and use a tool such as Balena Etcher to burn this to the internal storage.

Make sure to eject the mounted volume and power off the CM4 IO Board.

You can now remove the jumper to allow booting from the eMMC if used.

## To install the UpTallyFirmware firmware on a Grisp2 board:

- `export MIX_TARGET=grisp2`

- Install dependencies with `mix deps.get`

- Create firmware with `mix firmware`

If this is being installed onto a clean board:

- generate a firmware img `mix firmware.image`

- gzip it `gzip up_tally_firmware.img`

- copy it to a fat32 formwatted SD card, and insert into Grisp2 board

- Connect Grisp2 board to a computer using a USB cable

- Immediatley Open a serial console to the board `picocom /dev/tty.usbserial-011571 --baud 115200` and interrupt the boot sequence by pressing any key.

- Load the SD card by entering `ls mnt`

- Copy the image to the emmc `uncompress /mnt/mmc/up_tally_firmware.img.gz /dev/mmc1` then `reset`

- Board will now restart, run Ecto migrations and load the application

## Updating

If the board already has a running image:

- Generate the firmware `mix firmware`

- Upload it to the board with `./upload.sh`

> This assumes the board is on IP address 192.168.10.25, if not update the IP address in the upload.sh file

- Board will restart

## Resetting

### Pi4 CM

To reset all data on the Pi4 boot the device into USB Storage mode as above and use disk utility to format the SD card, or just refresh the img file manually.

### Grisp 2

To clear all existing persisted data follow these steps:

- Connect the board to a computer vua USB and allow the board to boot.

- Open a serial console to the board `picocom /dev/tty.usbserial-011571 --baud 115200` and interrupt the boot sequence by pressing any key.

- Corrupt the application folder to force a rebuild `dd if=/dev/zero of=/dev/mmcblk1p3 bs=128K count=1`

- Restart the board

## Accessing the system to logs and other useful info

To access the underlying Elixir system on the Pi4 you can ssh into it, specifying the ssh key located in this repository.

- `ssh -o "IdentitiesOnly=yes" -i ./up_tally_id_rsa 192.168.10.25`

Once connected you will be at the an iex prompt, to attach and view live logs run the command `log_attach` and return

There are other tools available, such a RingLogger that allow you to view old log messages, as well a network diagnostic tools and some basic linux shell command, for more information visit [https://hexdocs.pm/nerves/1.7.16/using-the-cli.html](https://hexdocs.pm/nerves/1.7.16/using-the-cli.html)

## Learn more

- Official docs: https://hexdocs.pm/nerves/getting-started.html

- Official website: https://nerves-project.org/

- Forum: https://elixirforum.com/c/nerves-forum

- Discussion Slack elixir-lang #nerves ([Invite](https://elixir-slackin.herokuapp.com/))

- Source: https://github.com/nerves-project/nerves

- Nerves System Grisp 2 https://github.com/nerves-project/nerves_system_grisp2
