defmodule UpTallyFirmware.Lights do
  use GenServer

  require Logger

  @phoenix_channel "status"

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def blip() do
    GenServer.cast(__MODULE__, :blip)
  end

  def init(_opts) do
    Logger.debug("Starting status lights server")

    case Phoenix.PubSub.subscribe(UpTally.PubSub, @phoenix_channel) do
      :ok ->
        Logger.debug("Subscribed to Phoenix Pubsub channel #{@phoenix_channel}")
        {:ok, %{}}

      {:error, term} ->
        Logger.error("Failed to staert status lights server.")
        IO.inspect(term)
        {:stop, term}
    end
  end

  def handle_cast(:blip, state) do
    Delux.render(Delux.Effects.blip(:on, :off))
    {:noreply, state}
  end

  def handle_info({:atem_status, status}, state) do
    case status do
      :connecting ->
        Delux.render(%{:atem => Delux.Effects.blink(:on, 500)}, :status)

      :connected ->
        Delux.render(%{:atem => Delux.Effects.on(:on)}, :status)

      :disconnected ->
        Delux.render(%{:atem => Delux.Effects.off()}, :status)

      # :transmitting ->
      #   Delux.render(%{:atem => Delux.Effects.blip(:on, :off)}, :notification)

      :recieving ->
        Delux.render(%{:atem => Delux.Effects.blip(:on, :off)}, :notification)

      _ ->
        nil
    end

    {:noreply, state}
  end

  def handle_info({:tsl_status, status}, state) do
    case status do
      # :connecting ->
      #   Delux.render(%{:tsl => Delux.Effects.blip(:on, :off)})

      # :connected ->
      #   Delux.render(%{:tsl => Delux.Effects.on(:on)}, :status)

      # :disconnected ->
      #   Delux.render(%{:tsl => Delux.Effects.off()}, :status)

      :transmitting ->
        Delux.render(%{:tsl => Delux.Effects.blip(:on, :off)}, :notification)

      # :recieving ->
      #   Delux.render(%{:tsl => Delux.Effects.blip(:on, :off)})

      _ ->
        nil
    end

    {:noreply, state}
  end

  def handle_info(_event, state) do
    {:noreply, state}
  end
end
